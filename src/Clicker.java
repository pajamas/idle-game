import java.util.Scanner;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class Clicker {
    public static boolean gameLoop = true;
    public static Bank bank = new Bank();
    public static Shop shop = new Shop();
    public static UI ui = new UI();
    
    public static void main(String[] args) {
        shop.addItem("mouse",new Item("Mouse",400,1,"\033[38;5;215m"));
        shop.addItem("sword",new Item("Sword",1500,7,"\033[38;5;215m"));
        shop.addItem("elephant",new Item("Elephant",5000,20,"\033[38;5;215m"));

        ui.start();
        ui.printShop(shop.getItems());
        
        Looper looper = new Looper();
        CommandGetter commandGetter = new CommandGetter();
        looper.start();
        commandGetter.start();
    }

    static class Looper extends Thread {
        Looper() {
        }   
        public void run() {
            ui.printMessage("press enter to make money, say exit to exit game",0,5);
            while (gameLoop) {
                ui.updateMoney(bank.getMoney());
                bank.earn(bank.getMPTOS());
                try {
                    Thread.sleep(100);
                } catch(Exception e){ System.out.println(e); }
            }
        }   
    };   

    static class CommandGetter extends Thread {
        Scanner commandLine = new Scanner(System.in);
        // arrow key character list
        private String[] banned = {"\033[A","\033[B","\033[C","\033[D"};

        CommandGetter() {
        }

        public void run() {
            while(gameLoop) {
                String command = commandLine.nextLine();
                // remove arrow key characters
                for (String code : this.banned) {
                    command = command.replace(code,"");
                }

                command = command.trim();

                // write command to line 5
                ui.resetCursor();
                ui.eraseLine();
                ui.printMessage(command,0,5);

                doCommand(command);
            }
        }

        public void doCommand(String command) {
            // what command
            if (command.equals("")) {
                bank.earn(1);
            }
            else if (command.equals("exit") || command.equals(":q")) {
                gameLoop = false;
                ui.end();
            }

            String[] parts = command.split(" ");
            
            if (parts[0].equals("buy") && parts.length>1) {
                String itemName = parts[1].toLowerCase();
                
                if (!shop.hasItem(itemName)) {
                    ui.printError("no such item in shop!");
                    return;
                }
                if (!bank.canAfford(shop.getPrice(itemName))) {
                    ui.printError("you don't have enough money!");
                    return;
                }
                bank.buyItem(shop.getItem(itemName));
                shop.buyItem(itemName);
                ui.printMessage("bought " + itemName);
                ui.updateMPS(bank.getMPTOS());
                ui.printShop(shop.getItems());
            }
        }
    };

    public static void write(String str) throws IOException {
        // for debugging
        BufferedWriter writer = new BufferedWriter(new FileWriter("out.txt"));
        writer.write(str);
    
        writer.close();
    }
}
