import java.util.ArrayList;
import java.util.HashMap;

public class Shop {
    private HashMap<String, Item> items = new HashMap<String, Item>();

    public void addItem(String name, Item item) {
        items.put(name, item);
    }

    public boolean hasItem(String item) {
        return items.containsKey(item);
    }
    public Item getItem(String item) {
        return items.get(item);
    }
    
    public ArrayList<Item> getItems() {
        return new ArrayList<>(items.values());
    }

    public int getPrice(String item) {
        return items.get(item).getPrice();
    }

    public int getMPTOS(String item) {
        return items.get(item).getMPTOS();
    }

    public void buyItem(String name) {
        items.get(name).increasePrice();
    }
}
