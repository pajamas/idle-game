public class Item {
    private String name;
    private int price;
    private int mptos;
    private String color;

    Item(String name, int price, int mptos, String color) {
        this.name = name;
        this.price = price;
        this.mptos = mptos;
        this.color = color;
    }

    public String getName() {
        return this.name;
    }
    public int getPrice() {
        return this.price;
    }
    public void increasePrice() {
        this.price *= 1.25;
    }
    public void upgrade() {
        this.mptos *= 1.20;
    }
    public int getMPTOS() {
        return this.mptos;
    }
    public String getColor() {
        return this.color;
    }
}
