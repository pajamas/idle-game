import java.util.ArrayList;

public class UI {
    public String errorColor = "\033[38;5;203m";
    public String defaultColor = "\033[0m";
        
    private String name;

    public UI() {
    }

    public void start() {
        // erase whole screen and go to 0,0
        clear();
        resetCursor();
        updateMoney(0);
        updateMPS(0);
    }

    public synchronized void updateMoney(int money) {
        // update money amount
        saveCursorPos();
        moveCursor(0,2);
        eraseLine();
        System.out.printf("\033[38;5;211mclicker game\033[0m, money: " + money/100.0 );
        restoreCursorPos();
    }

    public synchronized void updateMPS(int mptos) {
        saveCursorPos();
        System.out.printf("\033[3;0H\033[38;5;42mmoney per second: " + mptos/10.0 + "\033[0m");
        restoreCursorPos();
    }

    public synchronized void printError(String message) {
        resetCursor();
        setColor(errorColor);
        System.out.printf(message+" (press enter)");
        setColor(defaultColor);
        resetCursor();
    }

    public synchronized void printMessage(String message, int x, int y) {
        saveCursorPos();
        moveCursor(x, y);
        eraseLine();
        System.out.printf(message);
        restoreCursorPos();
    }

    public synchronized void printMessage(String message) {
        resetCursor();
        System.out.printf(message);
        resetCursor();
    }

    public void end() {
        clear();
        resetCursor();
        System.out.printf("Bye!\n");
    }

    public synchronized void moveCursor(int y, int x) {
        System.out.printf(String.format("\033[%s;%sH", x, y));
    }
    
    public synchronized void saveCursorPos() {
        System.out.printf("\0337");
    }

    public synchronized void restoreCursorPos() {
        System.out.printf("\0338");
    }

    public synchronized void resetCursor() {
        System.out.printf("\033[H");
    }

    public synchronized void clear() {
        System.out.printf("\033[2J");
    }

    public synchronized void eraseLine() {
        System.out.printf("\033[2K");
    }

    public synchronized void setColor(String color) {
        System.out.printf(color);
    }

    public synchronized void printShop(ArrayList<Item> items) {
        saveCursorPos();
        moveCursor(0,8);
        System.out.printf("  SHOP          command to buy: buy {itemname}");
        for (Item item : items)  {
            System.out.printf("\033[2E\033[2K" + item.getColor() + item.getName() + "\033[0m" + ": " + item.getPrice()/100.0+"$");
            System.out.printf("\033[1Emoney per second: " + item.getMPTOS()/10.0+"$");
        }
        restoreCursorPos();
    }
}
