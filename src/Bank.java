public class Bank {
    private int money = 0;
    private int moneyPerTenthOfSecond = 0;

    public int getMoney() {
        return this.money;
    }
    public boolean canAfford(int amount) {
        return money >= amount;
    }
    public int getMPTOS() {
        return this.moneyPerTenthOfSecond;
    }
    public void increaseMPTOS(int increment) {
        this.moneyPerTenthOfSecond += increment;
    }
    public int earn(int money) {
        this.money += money;
        return this.money;
    }
    public int spend(int money) {
        this.money -= money;
        return this.money;
    }
    public void buyItem(Item item) {
        spend(item.getPrice());
        increaseMPTOS(item.getMPTOS());
    }
}
